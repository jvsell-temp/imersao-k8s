# Desafios de Kubernetes

## Desafios Iniciais

### 1. Configurar o Minikube
**Objetivo:** Instalar e configurar o Minikube em suas máquinas locais.
- **Passos:**
  - Instalar o Minikube e o kubectl.
  - Iniciar um cluster local com o Minikube.
  - Verificar o estado do cluster com `kubectl cluster-info`.

### 2. Criar um Pod simples
**Objetivo:** Criar e gerenciar um Pod.
- **Passos:**
  - Escrever um manifesto YAML para criar um Pod que execute uma aplicação simples (por exemplo, Nginx).
  - Aplicar o manifesto usando `kubectl apply -f`.
  - Verificar o estado do Pod com `kubectl get pods` e `kubectl describe pod <nome-do-pod>`.

### 3. Escalar um Deployment
**Objetivo:** Criar um Deployment e escalá-lo.
- **Passos:**
  - Escrever um manifesto YAML para criar um Deployment com 3 réplicas.
  - Aplicar o manifesto e verificar os pods criados.
  - Escalar o Deployment para 5 réplicas e verificar novamente.

## Desafios Intermediários

### 4. Expor uma Aplicação com um Service
**Objetivo:** Expor uma aplicação rodando em um Pod para acesso externo.
- **Passos:**
  - Criar um Service do tipo NodePort para um Deployment existente.
  - Verificar o serviço e acessar a aplicação via navegador ou `curl`.

### 5. Usar ConfigMaps e Secrets
**Objetivo:** Injetar configuração e segredos em um Pod.
- **Passos:**
  - Criar um ConfigMap para armazenar variáveis de ambiente.
  - Criar um Secret para armazenar informações sensíveis.
  - Modificar um Pod para usar o ConfigMap e o Secret.

### 6. Atualizar um Deployment com Zero Downtime
**Objetivo:** Fazer uma atualização contínua de uma aplicação sem tempo de inatividade.
- **Passos:**
  - Criar um Deployment com uma aplicação básica.
  - Atualizar a imagem da aplicação no Deployment.
  - Verificar que a atualização foi feita sem interrupção do serviço.

## Desafios Avançados

### 7. Implementar um Ingress Controller
**Objetivo:** Configurar um Ingress Controller e criar regras de Ingress.
- **Passos:**
  - Instalar um Ingress Controller (por exemplo, Nginx Ingress Controller).
  - Criar regras de Ingress para rotear tráfego para diferentes serviços baseados em caminhos ou subdomínios.

### 8. Persistir Dados com Persistent Volumes
**Objetivo:** Utilizar Persistent Volumes (PVs) e Persistent Volume Claims (PVCs) para persistência de dados.
- **Passos:**
  - Criar um PV e um PVC.
  - Modificar um Pod para usar o PVC como um volume montado.
  - Verificar a persistência de dados através de reinicializações de Pods.

### 9. Monitoramento e Logs
**Objetivo:** Configurar ferramentas de monitoramento e log.
- **Passos:**
  - Instalar o Prometheus para monitoramento.
  - Configurar o Grafana para visualizar métricas.
  - Configurar o EFK (Elasticsearch, Fluentd, Kibana) stack para logs.

### 10. Gerenciamento de Recursos e Autoescalamento
**Objetivo:** Implementar o gerenciamento de recursos e autoescalamento.
- **Passos:**
  - Definir solicitações e limites de recursos para um Deployment.
  - Configurar um Horizontal Pod Autoscaler (HPA) baseado no uso de CPU ou memória.

## Desafio Final

### 11. Projeto Final - Desploy de Aplicação Completa
**Objetivo:** Implantar uma aplicação completa utilizando os conceitos aprendidos.
- **Passos:**
  - Dividir os alunos em grupos e fornecer uma aplicação que inclui vários serviços (por exemplo, front-end, back-end, banco de dados).
  - Cada grupo deve configurar e implantar a aplicação completa, incluindo deployments, serviços, volumes persistentes, ingress, e monitoramento.
